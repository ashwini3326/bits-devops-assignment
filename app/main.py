from fastapi import FastAPI
from random import randint 
import logging 

logging.basicConfig(level=logging.DEBUG)

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello, World!"}


@app.get("/randomNumber")
async def get_rand():
    random_number = randint(0, 150)
    logging.info(f"Generating a random number : {random_number}")
    return {"random_number": random_number}