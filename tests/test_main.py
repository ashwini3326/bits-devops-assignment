
from app.main import get_rand
import logging
import pytest

@pytest.mark.asyncio
async def test_get_rand(caplog):
    with caplog.at_level(logging.DEBUG):
        await get_rand()
        assert " Generating a random number" in caplog.text